Auto close comments
	close after X days inactivity, or
	close after X days
	reopen comments up to X days old
	
Wide Open Threads
	Checkbox on node create/edit form to automatically approve all comments submitted to the thread
	Screen to review and close wide open threads
	
Devoweling filter
	removes all the vowels from text enclosed in [devowel][/devowel] pseudotags
	if no psuedotags exist in the text, removes the vowels from the entire text
	
Blocks
	Live Discussions
