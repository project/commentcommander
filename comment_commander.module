<?php
/**@file
 * comment_commander.module
 * A bag of comment utilities, like
 *	- Automatically close comments on nodes beyond a configurable age
 *  - Devowelling filter
 *  - Live discussions block
 */

/**
 * Autoclose settings subform
 *
 * @return array form definition
 */
function _comment_commander_autoclose_settings() {
  // list of node types to affect
  $nodetypes = _comment_commander_nodeoptions(node_get_types());
  $cc_form['autoclose'] = array(
      '#type' => 'fieldset',
      '#title' => t('Autoclose comments'),
      '#collapsible' => TRUE
  ); //  	'#collapsed' => TRUE,
  $cc_form['autoclose']['comment_commander_autoclose_types'] = array(
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => t('Node types'),
      '#default_value' => variable_get('comment_commander_autoclose_types', $nodetypes),
      '#options' => $nodetypes
  );
  $cc_form['autoclose']['comment_commander_autoclose_inactive_days'] = array(
      '#type' => 'textfield',
      '#title' => t('Close when inactive for'),
      '#size' => 3,
      '#default_value' => variable_get('comment_commander_autoclose_inactive_days', '7'),
      '#suffix' => '<span> days</span>'
  );
  $cc_form['autoclose']['comment_commander_autoclose_age'] = array(
      '#type' => 'textfield',
      '#title' => t('Close when node age reaches'),
      '#size' => 3,
      '#default_value' => variable_get('comment_commander_autoclose_age', '7'),
      '#suffix' => '<span> days</span>'
  );
  $cc_form['mass_manage'] = array(
      '#type' => 'fieldset',
      '#title' => t('Manage comment statuses'),
      '#collapsible' => TRUE,
  		'#collapsed' => TRUE,
  ); 
  $cc_form['mass_manage']['autoclose'] = array(
      '#type' => 'submit',
      '#value' => t('Autoclose comments'),
      '#submit' => array('_comment_commander_autoclose_cron'),
  ); 
    $cc_form['mass_manage']['autoreset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset comments'),
      '#submit' => array('_comment_commander_autoclose_uncron'),
    ); 
  return $cc_form;
}

/**
 * Called by comment_commander_cron() to auto close comments
 *
 */
function _comment_commander_autoclose_cron() {
  //set it up
  $nodetypes = _comment_commander_nodeoptions(node_get_types());  
  $process_node_types = variable_get('comment_commander_autoclose_types', $nodetypes);
  if (count($process_node_types)) {
    $node_type_query_str = db_placeholders($process_node_types, 'text');
    $process_node_type_list = implode(',', $process_node_types);
    $inactive_days[] = variable_get('comment_commander_autoclose_inactive_days', '7');
    
    // knock it out    
    $qstr = 'UPDATE {node} SET comment = 1 WHERE nid IN
							(SELECT nid FROM {node_comment_statistics}
							WHERE last_comment_timestamp <= UNIX_TIMESTAMP() - 60*60*24*%d'. 
              ' AND type IN ('. $node_type_query_str .'))';
    db_query($qstr, array_merge($inactive_days, $process_node_types));
    
    // reopen Wide Open Threads
    $qstr = 'UPDATE {node} SET comment = 2 WHERE nid IN (SELECT nid FROM {comment_commander_openthread})';
    db_query($qstr);
    
    cache_clear_all();
  }
}

function _comment_commander_autoclose_uncron() {
  //set it up
  $nodetypes = _comment_commander_nodeoptions(node_get_types());  
  $process_node_types = variable_get('comment_commander_autoclose_types', $nodetypes);
  if (count($process_node_types)) {
    $node_type_query_str = db_placeholders($process_node_types, 'text');
    $process_node_type_list = implode(',', $process_node_types);
    $inactive_days[] = variable_get('comment_commander_autoclose_inactive_days', '7');
    
    // knock it out
    $qstr = 'UPDATE {node} SET comment = 2 WHERE nid IN
							(SELECT nid FROM {node_comment_statistics}
							WHERE last_comment_timestamp >= UNIX_TIMESTAMP() - 60*60*24*%d'. 
              ' AND type IN ('. $node_type_query_str .'))';
    db_query($qstr, array_merge($inactive_days, $process_node_types));
    
    // reopen Wide Open Threads that are older than %inactive days
    $qstr = 'UPDATE {node} SET comment = 2 WHERE nid IN (SELECT nid FROM {comment_commander_openthread})';
    
    cache_clear_all();
  }
}

/**
 * strip vowels from string
 * - searches for [devowel](.*)[/devowel], if found devowel the captured string, else devowel the whole string
 *
 * @param string $text
 * @return string
 */
function _comment_commander_devoweler($text) {
  $out = array();
  preg_match_all('@\[devowel](.+)\[/devowel]@U', $text, $out);
  $found_count = count($out[0]);
  if ($found_count) {
    for ($i = 0; $i < $found_count; $i++) {
      $text = str_replace($out[0][$i], '<span class="devoweled">' . preg_replace('@[aeiouAEIOU]@se', ' ', strip_tags($out[1][$i])) . '</span>', $text);
    }
  }
  else {
    $text = '<span class="devoweled">' . preg_replace('@[aeiouAEIOU]@se', ' ', strip_tags($text)) . '</span>';
  }
  return $text;
}

/**
 * Live discussions configuration form
 *
 * @param array $_comment_commander_livediscussion_config
 * @return array
 */
function _comment_commander_livediscussion_settings($_comment_commander_livediscussion_config) {
  $output['comment_commander_livediscussion_link_count'] = array(
    '#type' => 'select',
    '#title' => t('Number of recently commented posts to display'),
    '#default_value' => $_comment_commander_livediscussion_config['comment_commander_livediscussion_link_count'] ? $_comment_commander_livediscussion_config['comment_commander_livediscussion_link_count'] : 3,
    '#options' => drupal_map_assoc(array(
        5,
        10,
        15,
        20,
        25,
        30
    )),
    '#attributes' => 'size="4"'
  );

  $output['comment_commander_livediscussion_show_count'] = array(
    '#type' => 'radios',
    '#title' => t('Show comment count per post'),
    '#default_value' => $_comment_commander_livediscussion_config['comment_commander_livediscussion_show_count'] ? $_comment_commander_livediscussion_config['comment_commander_livediscussion_show_count'] : 'Yes',
    '#options' => array(
      'Yes' => 'Yes',
      'No' => 'No'
    )
  );
  return $output;
}

/**
 * Generate live discussions block
 *
 * @param array $_comment_commander_livediscussion_config
 * @return array
 */
function _comment_commander_livediscussion_view($_comment_commander_livediscussion_config) {
  // our block content
  // create block contents
  $show_comment_count = $_comment_commander_livediscussion_config['comment_commander_livediscussion_show_count'] ? $_comment_commander_livediscussion_config['comment_commander_livediscussion_show_count'] : 'Yes';
  $commented_limit = $_comment_commander_livediscussion_config['comment_commander_livediscussion_link_count'] ? $_comment_commander_livediscussion_config['comment_commander_livediscussion_link_count'] : 15;

  // create the SQL
  $query = "SELECT cs.nid, n.title, cs.comment_count, cs.last_comment_timestamp
           FROM {node_comment_statistics} cs
           LEFT JOIN {node} n on n.nid = cs.nid
           WHERE n.status = 1 AND cs.comment_count > 0
  				 ORDER BY cs.last_comment_timestamp DESC";

  $query_result = db_query_range(db_rewrite_sql($query), 0, $commented_limit);
  $user = $GLOBALS['user'];
  $comments_per_page = $user->comments_per_page ? $user->comments_per_page : ($_SESSION['comment_comments_per_page'] ? $_SESSION['comment_comments_per_page'] : variable_get('comment_default_per_page', '50'));
  // for each record create a link
  while ($outnode = db_fetch_object($query_result)) {
    $start_comment = $outnode->comment_count - ($outnode->comment_count % $comments_per_page);
    $link_q = ($comments_per_page < $outnode->comment_count) ? "from=$start_comment&comments_per_page=$comments_per_page" : NULL;
    $linktext[] = l($outnode->title . ($show_comment_count == 'Yes' ? ' (' . $outnode->comment_count . ')' : ''), "node/$outnode->nid", array(), $link_q, "comment");
  }
  if (count($linktext)) {
    $block['content'] = theme('item_list', $linktext);
  }
  $block['subject'] = 'Live Discussions';
  return $block;
}

/**
 * create array of nodetype strings for select control options
 *
 * @param array $nodetypes
 * @return array
 */
function _comment_commander_nodeoptions($nodetypes) {
  ksort($nodetypes);
  return drupal_map_assoc(array_keys($nodetypes));
}

/**
 * creates a string to add to the WHERE clause of a node SELECT query to filter by node type
 *
 * @param array $nodetypes
 * @return string
 */
function _comment_commander_node_select($nodetypes) {
  if (count($nodetypes)) {
    foreach ($nodetypes as $nodetype_index) {
      $node_condition[] = "(type='$nodetype_index')";
    }
    return " AND (" . implode(" OR ", $node_condition) . ')';
  }
  else return '';
}

/**
 * hook_block implementation
 *
 */
function comment_commander_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      // BLOCK_CACHE_PAGE in case I do something Organic Groupy
      $block['Live discussion'] = array(
          'info' => t("Live discussions"),
          'cache' => BLOCK_CACHE_PAGE
      );
      return $block;
      break;
    case 'configure':
      switch ($delta) {
        case 'Live discussion':
          $_comment_commander_livediscussion_config = variable_get('comment_commander_livediscussion_config', array());
          return _comment_commander_livediscussion_settings($_comment_commander_livediscussion_config);
          break;
        default:
          break;
      }
      break;
    case 'save':
      switch ($delta) {
        case 'Live discussion':
          $_comment_commander_livediscussion_config = variable_get('comment_commander_livediscussion_config', array());
          $_comment_commander_livediscussion_config['comment_commander_livediscussion_link_count'] = $edit['comment_commander_livediscussion_link_count'];
          $_comment_commander_livediscussion_config['comment_commander_livediscussion_show_count'] = $edit['comment_commander_livediscussion_show_count'];
          variable_set('comment_commander_livediscussion_config', $_comment_commander_livediscussion_config);
          break;
        default:
          break;
      }
      break;
    case 'view':
      switch ($delta) {
        case 'Live discussion':
          $_comment_commander_livediscussion_config = variable_get('comment_commander_livediscussion_config', array());
          return _comment_commander_livediscussion_view($_comment_commander_livediscussion_config);
          break;
        default:
          break;
      }
      break;
  }
}

/**
 * hook_cron() implementation
 *
 */
function comment_commander_cron() {
  $current_date = time();
  $next_cycle_time = variable_get('comment_commander_autoclose_next_date', $current_date);
  if ($current_date >= $next_cycle_time) {
    _comment_commander_autoclose_cron();
  }
  $comment_commander_autoclose_next_date = $current_date + 60 * 60 * 24;
//  variable_set('comment_commander_autoclose_next_date', $comment_commander_autoclose_next_date);
}

/**
 * hook_filter() implementation
 *
 */
function comment_commander_filter($op, $delta = 0, $format = -1, $text = '') {
  switch ($op) {
    case 'no cache':
      switch ($delta) {
        case 0:
          return FALSE;
          break;
        default:
          return TRUE;
          break;
      }
      break;
    case 'list':
      return array(

          0 => t('Devowel')
      );
      break;
    case 'description':
      switch ($delta) {
        case 0:
          return t('Removes the vowels from text. Basically for comments.');
          break;
        default:
          ;
          break;
      }
      break;
    case "process":
      switch ($delta) {
        case 0:
          return _comment_commander_devoweler($text);
          break;
        default:
          ;
          break;
      }
      break;
    default:
      return $text;
  }
}

/**
 * hook_filter_tips() implementation
 *
 */
function comment_commander_filter_tips($delta, $format, $long = FALSE) {
  switch ($delta) {
    case 0:
      if ($long) {
        return t('Removes the vowels in text between [devowel]*[/devowel] \'tags\', if found. If no such tags are found, the full string is \'devowelled\'. Basically for comments.');
      }
      else {
        return t('Removes the vowels from text. Basically for comments.');
      }
      break;
    default:
      ;
      break;
  }
}

/**
 * hook_form_alter() implementation
 *
 */
function comment_commander_form_alter(&$form, &$form_state, $form_id) {
  if (strstr($form_id, '_node_form') and user_access('Set wide open thread')) {
    $node = $form['#node'];
    $form['opencomment'] = array(
      '#type' => 'fieldset',
      '#title' => 'Open thread',
      '#collapsible' => TRUE,
      '#collapsed' => ($node->comments_wide_open <> 1),
      '#weight' => $form['comment_settings']['#weight'] - 1
    );
    $form['opencomment']['comments_wide_open'] = array(
      '#type' => 'checkbox',
      '#title' => t('Auto approve comments'),
      '#description' => t('Automatically approve comments submitted on this node.'),
      '#default_value' => ($node->comments_wide_open) ? $node->comments_wide_open : FALSE
    );
  }  
  elseif ($form_id == 'user_profile_form') {
    $form['#validate'][] = '_comment_commander_user_form_validate';
  }
}

/**
 * hook_help() implementation
 *
 */
function comment_commander_help($path) {
  // TODO : advanced help
  switch ($path) {
    case "admin/help#comment_commander":
      $output = t('
      	<p>The Comment Commander module allows you to automatically close comments via a cron hook. You can select any combination of available node types to process.
      	Configure the module to close comments after a number of days of inactivity and schedule the closings to be done daily, weekly, monthly or annually.<p>
      	<p>Comment Commander also has a Devoweling filter. If the text contains [devowel]...[/devowel] pseudo-tags, it will devowel the enclosed text. Otherwise, it will devowel the entire text.</p>
      	<p>Users with the appropriate permissions can also override the "post comments without approval" on a node-by-node basis.</p>
      	<p>For your users, Comment Commander also creates several blocks:</p>
      	<ul><li>The Live discussions block (which presents the most recently commented-upon nodes)</li>
      	<li>Commenter Blogroll (which list links to users that comment on the site)
      	</ul>
      ');
      break;
    default:
      $output = "";
      break;
  }
  return $output;
}

/**
 * hook_menu() implementation
 *
 */
function comment_commander_menu() {
  $items['admin/settings/comment_commander'] = array(
    'title' => 'Comment Commander',
    'description' => 'Set age, frequency and types of nodes for which comments will be closed.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array(
      'comment_commander_settings'
    ),
    'access arguments' => array(
      'administer site configuration'
    ),
  );
  $items['admin/content/comment/wide_open'] = array(
    'title' => 'Wide open thread',
    'description' => 'Review and close Wide Open Threads.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array(
      'comment_commander_wide_open_thread_admin_form'
    ),
    'access arguments' => array(
      'administer site configuration'
    ),
    'type' =>MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * hook_nodeapi() implementation
 *
 */
function comment_commander_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    case 'insert':
      if ($node->comments_wide_open)
        db_query('INSERT INTO {comment_commander_openthread} (nid) VALUES (%d)', $node->nid);
      break;
    case 'update':
      db_query('DELETE FROM {comment_commander_openthread} WHERE nid = %d', $node->nid);
      if ($node->comments_wide_open)
        db_query('INSERT INTO {comment_commander_openthread} (nid) VALUES (%d)', $node->nid);
      break;
    case 'delete':
      db_query('DELETE FROM {comment_commander_openthread} WHERE nid = %d', $node->nid);
      break;
    case 'load':
      $openthread_query = db_query('SELECT nid FROM {comment_commander_openthread} WHERE nid = %d', $node->nid);
      if (db_fetch_object($openthread_query)) {
        $node->comments_wide_open = 1;
      }
      else {
        $node->comments_wide_open = 0;
      }
      break;
    default:
      break;
  }
}

/**
 * hook_perm() implementation
 *
 */
function comment_commander_perm() {
  return array('Set wide open thread');
}

/**
 * Module settings form
 *
 * @return array form definition
 */
function comment_commander_settings() {
  // in case I need to add configuration sections
  $form = array();
  $form = array_merge($form, _comment_commander_autoclose_settings());
  return system_settings_form($form);
}

/**
 * hook_theme() implementation
 *
 */
function comment_commander_theme($existing, $type, $theme, $path) {
  return array(
    'theme_comment_commander_wide_open_thread_admin_form' => array(
      'arguments' => array(
        'form' => NULL
      )
    ),
  );
}

/**
 * 
 * @param $form_state
 */
function comment_commander_wide_open_thread_admin_form($form_state) {
  // create pager_query
  $q = 'SELECT o.nid, s.last_comment_timestamp, s.comment_count, n.title, n.created
  FROM {comment_commander_openthread} o
  JOIN {node_comment_statistics} s on s.nid = o.nid
  JOIN {node} n on n.nid = o.nid';
  $wide_open_comments = pager_query($q, 10);
  
  // create the dread form
  // TODO: yes, I know, get rid of the HTML and make a theme function
  while ($table_row = db_fetch_object($wide_open_comments)) {
  	$table_rows[$table_row->nid]['nid'] = array(
  	  '#type' => 'checkbox',
  	  '#return_value' => $table_row->nid,
  	  '#prefix' => '<tr><th>',
  	  '#suffix' => '</th>',
  	);
  	$table_rows[$table_row->nid]['title'] = array(
  	  '#type' => 'markup',
  	  '#value' => l($table_row->title, "node/$table_row->nid"),
  	  '#prefix' => '<th>',
  	  '#suffix' => '</th>'
  	);
  	$table_rows[$table_row->nid]['created'] = array(
  	  '#type' => 'markup',
  	  '#value' => format_date($table_row->created, 'small'),
  	  '#prefix' => '<th>',
  	  '#suffix' => '</th>'
  	);
  	$table_rows[$table_row->nid]['last_comment_timestamp'] = array(
  	  '#type' => 'markup',
  	  '#value' => format_date($table_row->last_comment_timestamp, 'small'),
  	  '#prefix' => '<th>',
  	  '#suffix' => '</th>'
  	);
  	$table_rows[$table_row->nid]['comment_count'] = array(
  	  '#type' => 'markup',
  	  '#value' => (string) $table_row->comment_count,
  	  '#prefix' => '<th>',
  	  '#suffix' => '</th></tr>'
  	);
  }
//  if (count($table_rows)) {
  	$form = array(
  	  'header' => array(
  	    '#type' => 'markup',
  	    '#value' => '<tr><th></th><th>Thread title</th><th>Thread start date</th><th>Last comment date</th><th>Number of comments</th></tr>',
  	    '#prefix' => '<table>',
  	),
    	'wide_open_threads' => array(
    	  '#type' => 'fieldset',
    	  '#tree' => TRUE,
  	  	'#suffix' => '</table>',
      	'row' => $table_rows,
    	),
    	'submit' => array(
    	  '#type' => 'submit',
    	  '#value' => 'Close selected threads'
    	),
    	'pager' => array(
    	  '#type' => 'markup',
    	  '#value' => theme('pager'),
    	),
  	);
  	
//  }
  return $form;
}

function comment_commander_wide_open_thread_admin_form_submit($form, &$form_state) {
  if (count($form_state['values']['wide_open_threads']['row'])){
    foreach ($form_state['values']['wide_open_threads']['row'] as $row) if ($row['nid']) $selected_nids[] = $row['nid'];
    if (count($selected_nids)) {
    	db_query('DELETE FROM {comment_commander_openthread} WHERE nid IN( '. db_placeholders($selected_nids, 'int') .')', $selected_nids);
    	drupal_set_message('Selected Wide Open Threads were successfully closed.');
    }
  }
}

function theme_comment_commander_wide_open_thread_admin_form($form) {
  return drupal_render_form('comment_commander_wide_open_thread_admin_form', $form);
}

